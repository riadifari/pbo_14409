public class LSegitiga {

    double alas, tinggi;

    double Luas_Segitiga() {
        alas = 6;
        tinggi = 8;

        return alas * tinggi / 2;
    }

    void output() {
        System.out.println(Luas_Segitiga());
    }

    public static void main(String[] args) {
        LSegitiga luas = new LSegitiga();
        luas.output();
    }
}
