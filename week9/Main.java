package week9;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("===========================================");
        System.out.println("Hitung Bangun");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.println("3. Lingkaran");
        System.out.println("4. Segitiga");
        System.out.println("5. Kubus");
        System.out.println("6. Balok");
        System.out.println("7. Tabung");
        System.out.println("0. Exit");
        System.out.println("Pilih Bangun");
        System.out.println("===========================================");

        Scanner obj = new Scanner(System.in);
        int x = obj.nextInt();

        int BangunDatar = x;
        switch (BangunDatar) {
            case 1:
                BangunDatar bd1 = new BangunDatar(11);
                System.out.println("Luas Persegi = " + bd1.luasPersegi());
                System.out.println("Keliling Persegi = " + bd1.kelPersegi());
                break;
            case 2:
                BangunDatar bd2 = new BangunDatar(22, 11);
                System.out.println("Luas Persegi Panjang = " + bd2.luasPerPan());
                System.out.println("Keliling Persegi Panjang = " + bd2.kelPerPan());
                break;
            case 3:
                BangunDatar bd3 = new BangunDatar(10.2);
                System.out.println("Luas Lingkaran = " + bd3.luasLingkar());
                System.out.println("Keliling Langkaran = " + bd3.kelLingkar());
                break;
            case 4:
                BangunDatar bd4 = new BangunDatar(7.4, 6.7);
                System.out.println("Luas Segitiga = " + bd4.luasSegitiga());
                System.out.println("Keliling Segitiga = " + bd4.kelSegitiga());
                break;
            case 5:
                BangunRuang br1 = new BangunRuang(4);
                System.out.println("Volume Kubus = " + br1.volKubus());
                break;
            case 6:
                BangunRuang br2 = new BangunRuang(4, 5, 6);
                System.out.println("Volume Balok = " + br2.volBalok());
                break;
            case 7:
                BangunRuang br3 = new BangunRuang(3.0, 3.5);
                System.out.println("Volume Tabung = " + br3.volTabung(br3.getAlas(), br3.getTinggi()));
                break;
            case 0:
                System.out.println("Terima Kasih");
                break;
        }
    }
}