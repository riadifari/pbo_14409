package week9;

public class BangunRuang extends BangunDatar {

    private int tinggi;

    public BangunRuang(int sisi) {
        super(sisi);
    }

    public BangunRuang(int panjang, int lebar, int tinggi) {
        super(panjang, lebar);
        this.tinggi = tinggi;
    }

    public BangunRuang(double alas, double tinggi) {
        super(alas, tinggi);
    }

    public int volKubus() {
        return sisi * sisi * sisi;
    }

    public double volBalok() {
        return panjang * lebar * tinggi;
    }

    public double volTabung(double alas, double tinggi) {
        return pi * alas * alas * tinggi;
    }
}
